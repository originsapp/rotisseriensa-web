/**
 * Faz a validação do formulário.
 *
 * @param string id ID de onde será feito a vlaidação, normalmente é o ID do formulário
 */
function validaForm( id ) {
	id = '#' + id;

	var erros = 0;
	var arrErroCampos = new Array();

	// Verifica os input
	$( id + " div.form-group" ).each( function() {
		var inp = $( this ).find( "input[req=true]" );
		if( inp.length > 1 ) {
			inp = $( inp[1] );
		}
		if( inp.val() == '' ) {
			$( this ).removeClass( "has-success" );
			$( this ).addClass( "has-error" );
			erros = erros + 1;
			arrErroCampos.push( $( this ).closest( ".form-group " ).find( "label" ).html() );
		} else {
			if( $( this ).find( "span.help-block1" ).length == 0 && inp.length > 0 ) {
				$( this ).removeClass( "has-error" );
				$( this ).addClass( "has-success" );
			}
		}
	} );

	// Verifica o textarea
	$( id + " div.form-group" ).each( function() {
		var inp = $( "textarea[req=true]", this );
		if( inp.length > 1 ) {
			inp = $( inp[1] );
		}
		if( inp.val() == '' ) {
			$( this ).removeClass( "has-success" );
			$( this ).addClass( "has-error" );
			erros = erros + 1;
			arrErroCampos.push( $( this ).closest( ".form-group " ).find( "label" ).html() );
		} else {
			if( $( this ).find( "span.help-block1" ).length == 0 && inp.length > 0 ) {
				$( this ).removeClass( "has-error" );
				$( this ).addClass( "has-success" );
			}
		}
	} );

	// Valida campos do tipo select
	$( id + " select[req=true]" ).each( function() {
		var selectField = this,
			selectedOption = $( this ).find( ":selected" ).val();

		if( selectedOption == "" || typeof (selectedOption) == 'undefined' ) {
			if( $( this ).parent( 'div.input-group' ).parent( 'div.form-group' ).length == 1 ) {
				if( $( this ).attr( 'disabled' ) == undefined ) {
					$( this ).parent( 'div.input-group' ).parent( 'div.form-group' ).removeClass( 'has-success' );
					$( this ).parent( 'div.input-group' ).parent( 'div.form-group' ).addClass( 'has-error' );
					erros = erros + 1;
					arrErroCampos.push( $( this ).closest( ".form-group " ).find( "label" ).html() );
				}
			} else {
				$( this ).closest( 'div.form-group' ).removeClass( 'has-success' );
				$( this ).closest( 'div.form-group' ).addClass( 'has-error' );
				erros = erros + 1;
				arrErroCampos.push( $( this ).closest( ".form-group " ).find( "label" ).html() );
			}
		} else {
			if( $( this ).parent( 'div.input-group' ).parent( 'div.form-group' ).length == 1 ) {
				$( this ).parent( 'div.form-group' ).removeClass( 'has-error' );
				$( this ).parent( 'div.form-group' ).addClass( 'has-success' );
			} else {
				$( this ).parent( 'div.form-group' ).removeClass( 'has-error' );
				$( this ).parent( 'div.form-group' ).addClass( 'has-success' );
			}
		}
	} );

	// Valida campos do tipo checkbox
	$( id + ' input:checkbox[req=true]' ).each( function() {
		if( $( this ).is( ':checked' ) ) {
			$( this ).parent().removeClass( "radioObrigatorio" );
		} else {
			erros = erros + 1;
			$( this ).parent().addClass( "radioObrigatorio" );
		}
	} );

	// Utilizado para colocar o texto da label em vermelho caso o
	// campo seja requerido mas não está preenchido.
	$( 'div.form-group', id ).each( function() {
		var span = $( this ).find( "span.popovers" ), inp;
		if( $( span ).html() == '*' ) {
			inp = $( this ).find( "input.form-control[req='true']" );
			if( inp.length == 0 ) {
				inp = $( this ).find( ".selectpicker option:selected" );
				if( inp.length == 0 ) {
					inp = $( this ).find( "select[req='true'] option:selected" );
				}
			}
			if( inp.length > 1 ) {
				inp = $( inp[1] );
			}
			if( inp.length > 0 && (inp.val().trim() == '' || inp.val() === undefined || $( this ).find( "span.help-block1" ).length > 0) ) {
				setError( this, inp, true, false );
			} else {
				// Verifica se o input é do tipo select2me, corrige o problema de não inserir borda.
				inp = $( this ).find( '.select2me' );
				if( inp.length == 0 )
					setError( this, inp, false, false );
			}
		}
	} );

	// Verica se o select.selectpicker possui ou não valores para adicionar borda vermelha na tela
	$( id + ' div.form-group' ).each( function() {
		var span = $( this ).find( "span.popovers" ),
			// Verifica se nas div's possuí um filho select.selectpicker
			inp = $( this ).find( "select.selectpicker.form-control" );
		if( inp.length > 0 ) {
			// se sim, verifica se há uma opção selecionada
			inp = $( this ).find( "option:selected" );
			if( $( span ).html() == '*' && inp.length == 0 ) {
				setError( this, inp, true, true );
			}
		}
	} );

	function setError( div, inp, err, picker ) {
		if( err ) {
			$( div ).addClass( "has-error" );
			// Se for selectpicker adiciona a atributo style
			if( !picker ) {
				if( inp.length > 0 && inp[0].className != 'form-control tt-input' )
					inp.removeAttr( "style" );
				if( inp.selector == ":selected" && $( div ).find( "div.bootstrap-select" ).length == 1 ) {
					$( div ).find( "div.bootstrap-select" ).attr( 'style', 'border: 1px solid rgb(185, 74, 72) !important' );
				}
			} else
				$( div ).find( 'button.selectpicker' ).attr( 'style', 'border: 1px solid rgb(185, 74, 72) !important' );

			erros = erros + 1;
			arrErroCampos.push( $( this ).closest( ".form-group " ).find( "label" ).html() );
		} else {
			$( div ).removeClass( "has-error" );
			// Se for selectpicker remove a atributo style
			if( !picker ) {
				if( inp.selector == ":selected" && $( div ).find( "div.bootstrap-select" ).length == 1 ) {
					$( div ).find( "div.bootstrap-select" ).removeAttr( 'style' );
				}
			} else
				$( div ).find( 'button.selectpicker' ).removeAttr( 'style' );
		}
	}

	// Chama a função para validar o e-mail
	var erro_email = false;
	$( id + " .email" ).each( function() {
		if( $( this ).attr( "req" ) == 'true' || $( this ).val() != '' ) {
			var email = $( this ).val();

			var valida = valida_email( email );

			if( valida == false ) {
				erro_email = true;
				erros = erros + 1;
				$( this ).parent( 'div.form-group' ).addClass( "has-error" );
			} else {
				$( this ).parent( 'div.form-group' ).addClass( "has-success" );
			}
		}
	} );

	if( erros > 0 ) {
		if( erro_email ) {
			toastr.warning( "Preencha todos os campos obrigatórios" );
		} else {
			erros = '';

			// Grava em uma string os campos com erros do  array
			var msgErroCampos = "<br><br><strong>Campos Obrigatórios:</strong>";
			for( a = 0; a < arrErroCampos.length; a++ ) {
				// Remove os html e monta a mensagem
				if( arrErroCampos[a] != undefined ) {
					msgErroCampos += "<br>" + arrErroCampos[a].replace( /<[\s\S]*?>/g, "" );
				}
			}
			toastr.warning( "Preencha todos os campos obrigatórios" + erros + msgErroCampos );
			msgErroCampos = [];
		}
		if( $( id ).parent( 'div.modal-body' ).length == 0 ) {
			$( 'html, body' ).animate( {scrollTop: 0}, 'normal' );
		}

		return false;
	} else {
		return true;
	}
}