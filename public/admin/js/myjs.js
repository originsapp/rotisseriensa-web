var contador = 0;

$( document ).ready( function() {

	//começo loja
	$( "#ceploja" ).mask( "99999-999" );
	$( "#abtloja" ).mask( "99:99" );
	$( "#fchloja" ).mask( "99:99" );

	$( "#ufloja" ).attr( 'maxlength', '2' );
	$( "#nomeloja" ).attr( 'maxlength', '35' );
	$( "#munloja" ).attr( 'maxlength', '45' );
	$( "#brrloja" ).attr( 'maxlength', '50' );
	$( "#endloja" ).attr( 'maxlength', '75' );

	$( "#ufloja" ).keypress( function() {
		var tamanho = $( "#ufloja" ).val().length;
		if( tamanho == 2 ) {
			return false;
		}
	} );

	$( "#nomeloja" ).keypress( function() {
		var tamanho = $( "#nomeloja" ).val().length;
		if( tamanho == 35 ) {
			return false;
		}
	} );

	$( "#munloja" ).keypress( function() {
		var tamanho = $( "#munloja" ).val().length;
		if( tamanho == 45 ) {
			return false;
		}
	} );

	$( "#brrloja" ).keypress( function() {
		var tamanho = $( "#brrloja" ).val().length;
		if( tamanho == 50 ) {
			return false;
		}
	} );

	$( "#endloja" ).keypress( function() {
		var tamanho = $( "#endloja" ).val().length;
		if( tamanho == 75 ) {
			return false;
		}
	} );


	//começo usuário
	$( '#nomeusuario' ).on( 'keypress', function( event ) {
		var regex = new RegExp( "^[a-zA-Z]+$" );
		var key = String.fromCharCode( !event.charCode ? event.which : event.charCode );
		if( !regex.test( key ) ) {
			event.preventDefault();
			return false;
		}
	} );

	$( '#sobrenomeusuario' ).on( 'keypress', function( event ) {
		var regex = new RegExp( "^[a-zA-Z]+$" );
		var key = String.fromCharCode( !event.charCode ? event.which : event.charCode );
		if( !regex.test( key ) ) {
			event.preventDefault();
			return false;
		}
	} );

	$( "#nomeusuario" ).attr( 'maxlength', '20' );
	$( "#sobrenomeusuario" ).attr( 'maxlength', '20' );
	$( "#senhausuario" ).attr( 'maxlength', '20' );

	$( "#nomeusuario" ).keypress( function() {
		var tamanho = $( "#nomeusuario" ).val().length;
		if( tamanho == 20 ) {
			return false;
		}
	} );

	$( "#sobrenomeusuario" ).keypress( function() {
		var tamanho = $( "#sobrenomeusuario" ).val().length;
		if( tamanho == 20 ) {
			return false;
		}
	} );

	$( "#senhausuario" ).keypress( function() {
		var tamanho = $( "#senhausuario" ).val().length;
		if( tamanho == 20 ) {
			return false;
		}
	} );
	//fim ausuario



	//começo categoria
	$( "#titulocategoria" ).attr( 'maxlength', '35' );

	$( "#titulocategoria" ).keypress( function() {
		var tamanho = $( "#titulocategoria" ).val().length;
		if( tamanho == 35 ) {
			return false;
		}
	} );
	//fim categoria
	//
//cardapio
	$( "#adicionalinha" ).click( function() {
		$( '#recebelinha tr:last' ).after( '<tr id="tr' + contador + '">\n\
                                            <td><input type="text" class="form-control" name="misturacardapio[]"></td>\n\
                                            <td><select class="form-control" name="opcaocardapio[]"><option value="N">Não</option><option value="S">Sim</option></select></td>\n\
                                            <td class="text-center"><a onclick="deletalinha(tr' + contador + ')"><i class="icone iconevermelho fa fa-minus"></i></a></td>\n\
                                         </tr>' );

		contador++;

	} );

	$( "#cdpespecial" ).change( function() {

		var valor = $( "#cdpespecial" ).val();

		if( valor == 'S' ) {


			$( "#especialcardapio" ).attr( 'disabled', false );
			$( "#inpespecialcardapio" ).remove();

		} else {

			$( "#especialcardapio option" ).attr( 'selected', false );
			$( "#especialcardapio" ).attr( 'disabled', true );
			$( "#recebeespecial" ).append( '<input type="hidden" name="especialcardapio" id="inpespecialcardapio" value="==QM">' );
		}

	} );

	$( "#btnenviarform" ).click( function() {
		var msg = "", fields = document.getElementById( "formrnsa" ).getElementsByTagName( "input" );
		for( var i = 0; i < fields.length; i++ ) {

			if( fields[i].value == "" )
				msg += fields[i].title + ' is required. \n';

		}
		if( msg ) {
			toastr.warning( 'Preencha todos os campos antes de enviar o formulário' );
		} else {
			$( "#formrnsa" ).submit();
		}

	} );
	$( "#datacardapio" ).datepicker( {
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
		dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
		monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
		nextText: 'Próximo',
		prevText: 'Anterior'
	} );

	$( "#datacardapio" ).mask( '99/99/9999' );
	//fim cardapio

	// Permite somente números nos input com a classe .numeros
	somenteNumeros();

	//Adiciona mascara de moeda nos campos com a classe .moeda
	mascaraMoeda();

	// Configurações do datatable
	$( '#customDatatable' ).DataTable( {
		"language": {
			"url": "https://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
		},
		"pageLength": 50
	} );
} );

/**
 * Permite somente números nos input com a classe .numeros
 */
function somenteNumeros() {
	// Somente números
	$( ".numeros" ).keydown( function( event ) {
		// Allow: backspace, delete, tab, escape, enter and .
		if( $.inArray( event.keyCode, [46, 8, 9, 27, 13, 190] ) !== -1 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39) ) {
			// Allow: Ctrl+A => (event.keyCode == 65 && event.ctrlKey === true)
			// Allow: home, end, left, right => (event.keyCode >= 35 && event.keyCode <= 39)
			// let it happen, don't do anything
			return;
		} else {
			// Ensure that it is a number and stop the keypress
			if( event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) ) {
				event.preventDefault();
			}
		}
	} );
}

/**
 * Adiciona mascara de moeda nos campos com a classe .moeda
 */
function mascaraMoeda() {
	// Preenche todo campo de moeda com valor 0 antes de por a máscara, para ficar 0,00
	$( '.moeda' ).each( function() {
		if( $( this ).val() == '' ) {
			$( this ).val( '0' );
		}
	} );

	$( '.moeda' ).blur( function() {
		if( $( this ).val() == '' ) {
			$( this ).val( '0,00' );
		}
	} );

	// Máscara para moeda
	$( ".moeda" ).maskMoney( {symbol: "", decimal: ",", thousands: ".", allowZero: true} );
}

// Ajax para carregar cidades em formato <option> para abastecer um select

function carregaCidades( id_cidade, estado, seleciona_cidade ) {
	$( "#" + id_cidade ).html( '<option value="-1">Carregando...</option>' );

	$.ajax( {
		url: getSegment() + "cidade/montaComboBox/" + estado,
		async: false,
		success: function( data ) {
			$( "#" + id_cidade ).html( '<option value="">Selecione</option>' );
			$( "#" + id_cidade ).append( data );

			// Seleciona uma cidade
			if( seleciona_cidade != '' && seleciona_cidade != undefined ) {

				// Verifica se a string possui somente números
				if( !isNaN( seleciona_cidade ) ) {
					// se somente número busca pelo id da cidade
					$( "#" + id_cidade ).val( seleciona_cidade );
				} else {
					// busca pelo valor do html exemplo 'Bauru'

					var val_cidade;
					$( "#" + id_cidade + " option" ).filter( function() {
						var text = $( this ).text();
						text = removeAcentos( text.toLowerCase() );
						seleciona_cidade = removeAcentos( seleciona_cidade.toLowerCase() );
						if( text === seleciona_cidade ) {
							val_cidade = $( this ).val();
							return;
						}
					} );

					$( "#" + id_cidade ).val( val_cidade );
				}
			}

		}
	} );
}

/**
 * Retorna a url atual ou retorna parte dela, a URL é separada por / em partes
 * @param {type} index Número da parte da URl que deseja que seja retornado começando do 0
 */
function getSegment( index ) {
	var url = window.location.pathname, host = window.location.host.charAt( 0 ).toLowerCase();
	if( url.search( "#" ) != -1 ) {
		url = url.replace( '#', '' );
	}
	url = url.substr( 1, url.length ).split( '/' );
	if( !isNaN( index ) ) {
		if( url[index] ) {
			if( host != 'u' && host != '1' ) {
				return url[index];
			} else {
				return url[++index];
			}
		} else {
			return '';
		}
	} else {
		if( host != 'u' && host != '1' ) {
			url = window.location.origin + '/';
		} else {
			url = window.location.origin + '/' + url[0] + '/';
		}
	}
	return url;
}

/**
 * * Função para remover os acentos
 *
 * @param {string} texto
 */
function removeAcentos( texto ) {
	var rExps = [
		{re: /[\xC0-\xC6]/g, ch: "A"},
		{re: /[\xE0-\xE6]/g, ch: "a"},
		{re: /[\xC8-\xCB]/g, ch: "E"},
		{re: /[\xE8-\xEB]/g, ch: "e"},
		{re: /[\xCC-\xCF]/g, ch: "I"},
		{re: /[\xEC-\xEF]/g, ch: "i"},
		{re: /[\xD2-\xD6]/g, ch: "O"},
		{re: /[\xF2-\xF6]/g, ch: "o"},
		{re: /[\xD9-\xDC]/g, ch: "U"},
		{re: /[\xF9-\xFC]/g, ch: "u"},
		{re: /[\xC7-\xE7]/g, ch: "c"},
		{re: /[\xD1]/g, ch: "N"},
		{re: /[\xF1]/g, ch: "n"}
	];

	$.each( rExps, function() {
		texto = texto.replace( this.re, this.ch );
	} );

	return texto;
}

function deletalinha( linha ) {

	$( linha ).remove();

}