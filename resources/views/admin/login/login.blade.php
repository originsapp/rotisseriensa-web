<!DOCTYPE html>

<html lang="pt-br">
    
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Rotisserie NSA - Login</title>

        
        <link href="{{url('admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{url('admin/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{url('admin/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
        <link href="{{url('admin/vendors/animate.css/animate.min.css')}}" rel="stylesheet">
        <link href="{{url('admin/build/css/custom.min.css')}}" rel="stylesheet">
        
    </head>

    <body class="login">
        
        <div>
            
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>

            <div class="login_wrapper">
                
                <div class="animate form login_form">
                    
                    <section class="login_content">
                        
                        <form method="post" action="{{url('/adm/login/logar')}}" class="form-horizontal">

                            {{ csrf_field() }}

                            <h1>Login</h1>

                            <div>
                                <input id="loginusuario" name="loginusuario" type="text" class="form-control" placeholder="Usuário">
                            </div>

                            <div>
                                <input id="loginsenha" name="loginsenha" type="password" class="form-control" placeholder="Senha">
                            </div>
                            
                            <div>
                                <button type="submit" class="btn btn-default submit">Entrar</button>
                                <a class="reset_pass">Perdeu a Senha?</a>
                            </div>

                            <div class="clearfix"></div>

                            <div class="separator">
                                
                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <h1><img src="{{url('img/icones/comida.png')}}" height="25px" width="25px"> Rotisserie NSA!</h1>
                                    <p>&copy;2017 - Ano Atual, Todos Direitos Reservados.</p>
                                    <p>Produzido por Origins APP.</p>
                                </div>
                                
                            </div>
                            
                        </form>
                        
                    </section>
                    
                </div>

            </div>
                        
        </div>

    </body>
</html>
