@extends('admin.template.template')

@section('admin')


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Menu</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="x_panel">
                <div class="x_title">

                    @if( isset($errors) && count($errors) > 0 )

                    <div class="alert alert-danger">

                        @foreach( $errors->all() as $err )

                        <p>{{$err}}</p>

                        @endforeach

                    </div>

                    @endif

                    <h2>{{$cdtalt}} de Menu</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">



                    @if( isset($menu) && count($menu) > 0 )

                    <form id="formulario" method="post" action="{{url('/adm/menu/alterar/alt/' . $id)}}" class="form-horizontal" onsubmit="return validaForm( this.id )">

                        @else

                        <form id="formulario" method="post" action="{{url('/adm/menu/cadastrar/cdt')}}" class="form-horizontal" onsubmit="return validaForm( this.id )">

                            @endif

                            {{ csrf_field() }}

                            <div class="col-md-8 center-margin">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Titulo</label>
                                            <input type="text" class="form-control" id="titulomenu" name="titulomenu" value="{{$menu->menu_titulo or old('titulomenu')}}" req="true">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Descrição</label>
                                            <input type="text" class="form-control" id="descricaomenu" name="descricaomenu" value="{{$menu->menu_descricao or old('descricaomenu')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Lojas</label>
                                            <select class="selectpicker form-control" multiple data-actions-box="true" name="lojasmenu[]" req="true">
                                                @foreach($lojas as $l)
												@php
												$selected = '';
												if (!empty($menu_loja)){
													$resultado = $menu_loja->where('loja_id', descodificaString($l->loja_id))->toArray();
													if( !empty($resultado)) {
														$selected = 'selected';
													} else {
														$selected = '';
													}
												}
												@endphp
                                                <option value="{{$l->loja_id}}" {{$selected}}>{{$l->loja_nome}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Categoria</label>
											{!! Form::select('categoriamenu', $categorias, $categoriaSelecionada, ['class' => 'form-control', 'req' => 'true'] ) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Preço</label>
                                            <input type="text" class="form-control moeda" id="precomenu" name="precomenu" value="{{$menu->menu_preco or old('precomenu')}}" req="true">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tipo preço</label>
                                            <input type="text" class="form-control" id="tipoprecomenu" name="tipoprecomenu" value="{{$menu->menu_tipopreco or old('tipoprecomenu')}}" req="true">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">{{$cdtaltr}}</button>
                                    @if( isset($menu) && count($menu) > 0 )
                                    <button type="button" data-toggle="modal" data-target="#modaldesativa" class="btn btn-danger">Desativar</button>
                                    @endif
                                </div>
                            </div>
                        </form>

                </div>

            </div>
        </div>
    </div>
</div>
@if( isset($menu) && count($menu) > 0 )
<div class="modal fade" tabindex="-1" role="dialog" id="modaldesativa">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Deseja mesmo Desativar essa Menu?</h4>
            </div>
            <div class="modal-body">
                <p>Desativar {{$menu->menu_titulo}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a type="button" href="{{url("/adm/menu/desativar/" . $id)}}" class="btn btn-danger">Desativar</a>
            </div>
        </div>
    </div>
</div>
@endif

@endsection
