@extends('admin.template.template')

@section('admin')


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Usuário</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="x_panel">
                <div class="x_title">

                    @if( isset($errors) && count($errors) > 0 )

					<div class="alert alert-danger">

						@foreach( $errors->all() as $err )

						<p>{{$err}}</p>

						@endforeach

					</div>

                    @endif

                    <h2>{{$cdtalt}} de Usuário</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

					@if( isset($usuarios) && count($usuarios) > 0 )

					<form id="formusuario" method="post" action="{{url('/adm/usuario/alterar/alt/' . $id)}}" class="form-horizontal" onsubmit="return validaForm( this.id )">

						@else

						<form id="formusuario" method="post" action="{{url('/adm/usuario/cadastrar/cdt')}}" class="form-horizontal" onsubmit="return validaForm( this.id )">

                            @endif

							{{ csrf_field() }}

							<div class="col-md-8 center-margin">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Nome</label>
											<input type="text" class="form-control" id="nomeusuario" name="nomeusuario" value="{{$name or old('nomeusuario')}}" req='true'>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Sobrenome</label>
											<input type="text" class="form-control" id="sobrenomeusuario" name="sobrenomeusuario" value="{{$sobre or old('sobrenomeusuario')}}" req='true'>
										</div>
									</div>
								</div>
								<div class="row">

									<div class="col-md-6">
										<div class="form-group">
											<label>Tipo de Usuário</label>
											<select name="tipousuario" class="form-control" req='true'>
												<option value="">Selecione</option>
												<option value="C"
														@if(isset($usutip) && $usutip == 'C')
														selected="selected"
														@endif
														>Comum</option>
												<option value="A"
														@if(isset($usutip) && $usutip == 'A')
														selected="selected"
														@endif
														>Administrador</option>
											</select>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Senha</label>
											<input type="password" class="form-control" id="senhausuario" name="senhausuario" req='true'>
										</div>
									</div>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-8 col-md-offset-2">
									<div class="col-md-2">
										<button id="btnusuario" type="submit" class="btn btn-primary">{{$cdtaltr}}</button>
									</div>
									@if( isset($usuarios) && count($usuarios) > 0 )
									<div class="col-md-2">
										<button type="button" data-toggle="modal" data-target="#modaldesativa" class="btn btn-danger">Desativar</button>
									</div>
									@endif
								</div>
							</div>
						</form>

                </div>
            </div>
        </div>
    </div>
</div>

@if( isset($usuarios) && count($usuarios) > 0 )
<div class="modal fade" tabindex="-1" role="dialog" id="modaldesativa">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Deseja mesmo Desativar esse Usuário?</h4>
			</div>
			<div class="modal-body">
				<p>Desativar {{$nome . " " . $sobrenome}}</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<a type="button" href="{{url("/adm/usuario/desativar/" . $id)}}" class="btn btn-danger">Desativar</a>
			</div>
		</div>
	</div>
</div>
@endif

@endsection
