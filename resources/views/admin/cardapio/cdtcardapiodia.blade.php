@extends('admin.template.template')

@section('admin')


<div class="right_col" role="main">
    <div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Cardápio</h3>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="x_panel">
				<div class="x_title">

					@if( isset($errors) && count($errors) > 0 )

					<div class="alert alert-danger">

						@foreach( $errors->all() as $err )

						<p>{{$err}}</p>

						@endforeach

					</div>

					@endif

					<h2>{{$cdtalt}} de Cardápio</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">

					@if( isset($mistura) && count($mistura) > 0 )

					<form id="formrnsa" method="post" action="{{url('/adm/cardapio/alterar/alt/' . $idcdp)}}" class="form-horizontal form-label-left">

						@else

						<form id="formrnsa" method="post" action="{{url('/adm/cardapio/cadastrar/cdt')}}" class="form-horizontal form-label-leftl">

							@endif

							{{ csrf_field() }}
							<div class="col-md-8 center-margin">

								<div class="form-group">

									<div class="row">

										<div class="col-md-4">
											<label>Descrição</label>
											<input type="text" class="form-control" id="desccardapio" name="desccardapio" value="{{$desc or old('nomeloja')}}">
										</div>

										<div class="col-md-4">
											<label>Salada</label>
											<input type="text" class="form-control" id="saladacardapio" name="saladacardapio" value="{{$sal or old('numloja')}}">
										</div>

										<div class="col-md-4">
											<label>Sobremesa</label>
											<input type="text" class="form-control" id="sobremesacardapio" name="sobremesacardapio" value="{{$sobr or old('ufloja')}}">
										</div>

									</div>

								</div>

								<div class="form-group">

									<div class="row">

										<div class="col-md-6">
											<label>É Cardápio Especial?</label>
											<select id="cdpespecial" class="form-control">
												<option value="N">Não</option>
												<option value="S"
														@if($idtipo != '==QM')
														selected="selected"
														@endif
														>Sim</option>
											</select>
										</div>

										<div id="recebeespecial" class="col-md-6">
											<label>Cardápios Especiais</label>
											<select class="form-control" id="especialcardapio" name="especialcardapio"
                                                    @if($idtipo == '==QM')
													disabled
                                                    @endif
                                                    >
                                                    @foreach($tipomarm as $tm)
                                                    <option value="{{$tm->tipo_id}}"
													@if($idtipo == $tm->tipo_id)
													selected='selected'
													@endif
													>{{$tm->tipo_nome}}</option>
												@endforeach
											</select>
											@if($idtipo == '==QM')
											<input type="hidden" name="especialcardapio" id="inpespecialcardapio" value="==QM">
											@endif
										</div>

									</div>

								</div>

								<br><br>

								<div class="form-group">

									<div class="row">

										<div class="col-md-12">
											<div class="table-responsive">

                                                <table id="recebelinha" class="table table-striped table-bordered jambo_table bulk_action">

                                                    <thead>
                                                        <tr>
                                                            <th class="col-md-6 text-center">Mistura</th>
                                                            <th class="col-md-4 text-center">É Opção?</th>
                                                            <th class="col-md-2 text-center">Ações</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @if( isset($mistura) )

                                                        @foreach($mistura as $m)

                                                        @if($conta == 0)

                                                        <tr>
                                                            <td> <input class="form-control" name="misturacardapio[]" value="{{$m->mst_desc}}"> </td>
                                                            <td>
                                                                <select class="form-control" name="opcaocardapio[]">
                                                                    <option value="N"
																			@if($m->mst_opc == 'N')
																			selected='selected'
																			@endif
																			>Não</option>
                                                                    <option value="S"
																			@if($m->mst_opc == 'S')
																			selected='selected'
																			@endif
																			>Sim</option>
                                                                </select>
                                                            </td>
                                                            <td class="text-center"><a id="adicionalinha"><i class="icone iconeverde fa fa-plus"></i></a></td>
                                                        </tr>

                                                        @else

                                                        <tr id="tr{{$conta}}">
                                                            <td><input type="text" class="form-control" name="misturacardapio[]" value="{{$m->mst_desc}}"></td>
                                                            <td>
                                                                <select class="form-control" name="opcaocardapio[]">
                                                                    <option value="N"
																			@if($m->mst_opc == 'N')
																			selected='selected'
																			@endif
																			>Não</option>
                                                                    <option value="S"
																			@if($m->mst_opc == 'S')
																			selected='selected'
																			@endif
																			>Sim</option>
                                                                </select>
                                                            </td>
                                                            <td class="text-center"><a onclick="deletalinha(tr{{$conta}})"><i class="icone iconevermelho fa fa-minus"></i></a></td>
                                                        </tr>

                                                        @endif

													<input type="hidden" value="{{$conta++}}">

													@endforeach

													@else
													<tr>
														<td> <input class="form-control" name="misturacardapio[]"> </td>
														<td>
															<select class="form-control" name="opcaocardapio[]">
																<option value="N">Não</option>
																<option value="S">Sim</option>
															</select>
														</td>
														<td class="text-center"><a id="adicionalinha"><i class="icone iconeverde fa fa-plus"></i></a></td>
													</tr>

													@endif

                                                    </tbody>

                                                </table>

                                            </div>
										</div>

									</div>

								</div>

								@if(isset($dia))
								<input type="hidden" name="lojacardapio" value="{{ $id or $loja }}">
								<input type="hidden" name="datacardapio" value="{{$dia}}">
								@else
								<div class="form-group">
									<div class="row">

										<div class="col-md-6">
											<label>Data de publicação</label>
											<input class="form-control" type="text" id="datacardapio" name="datacardapio" value="{{$data or ''}}">

										</div>

										<div class="col-md-6">
											<label>Loja</label>
											<select class="form-control" id="lojacardapio" name="lojacardapio">
												<option value="">Selecione</option>
												@foreach($lojas as $l)
												<option value="{{$l->loja_id}}"
														@if( isset( $loja ) && $loja == $l->loja_id)
														selected="selected"
														@endif
														>{{$l->loja_nome}}</option>
												@endforeach
											</select>

										</div>
									</div>
								</div>
								@endif

							</div>

							<div class="ln_solid"></div>

							<div class="form-group">
								<div class="col-md-8 col-md-offset-2">
									<button id="btnenviarform" type="button" class="btn btn-primary" style=>Cadastrar</button>
								</div>
							</div>
						</form>

				</div>

			</div>
		</div>
	</div>

</div>

@endsection
