@extends('admin.template.template')

@section('admin')


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Cardápio</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        
        
        
        
        <div class="row">
            <div class="x_panel">
                <div class="x_title">

                    @if( isset($errors) && count($errors) > 0 )

                    <div class="alert alert-danger">

                        @foreach( $errors->all() as $err )

                        <p>{{$err}}</p>

                        @endforeach

                    </div>

                    @endif

                    
                    
                    <h2>Cardápio</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                
                
                
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th class="column-title">Nome da Loja</th>
                                    <th class="column-title">Situação</th>
                                </tr>
                            </thead>

                            <tbody>
                            @if ( isset($lojas) && count($lojas) > 0 )

                            @foreach ( $lojas as $l )

                                <tr class="even pointer">

                                    <td>{{$l->loja_nome}}</td>
                                    <td><a href="{{url("/adm/cardapio/consultar/" . $l->loja_id)}}"> {{$l->situacao}} </a></td>
                                </tr>
                            @endforeach

                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
</div>



@endsection
