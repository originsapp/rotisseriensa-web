@extends('admin.template.template')

@section('admin')


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Loja</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="x_panel">
                <div class="x_title">

                    @if( isset($errors) && count($errors) > 0 )

					<div class="alert alert-danger">

						@foreach( $errors->all() as $err )

						<p>{{$err}}</p>

						@endforeach

					</div>

                    @endif

                    <h2>{{$cdtalt}} de Loja</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

					@if( isset($loja) && count($loja) > 0 )

					<form id="formloja" method="post" action="{{url('/adm/loja/alterar/alt/' . $id)}}" class="form-horizontal form-label-left" onsubmit="return validaForm( this.id )">

						@else

						<form id="formloja" method="post" action="{{url('/adm/loja/cadastrar/cdt')}}" class="form-horizontal form-label-leftl" onsubmit="return validaForm( this.id )">

							@endif

							{{ csrf_field() }}
							<div class="col-md-8 center-margin">
								<div class="form-group">
									<label>Nome</label>
									<input type="text" class="form-control" id="nomeloja" name="nomeloja" value="{{$loja->loja_nome or old('nomeloja')}}" req='true'>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label>CEP</label>
											<input type="text" id="ceploja" class="form-control" name="ceploja" value="{{$loja->loja_cep or old('ceploja')}}" req='true'>
										</div>
										<div class="col-md-6">
											<a id="btnsearchcep" class="btn btn-success" onclick="consultaCEP( this )" style="margin-top: 26px"><i class="fa fa-search"> Consultar CEP</i></a>
										</div>
									</div>
								</div>
								<div class="row">
									{{-- Cria o campo de UF e Municipio --}}
									{{endereco( '', $enderecoSelecionado, $estados, $cidades, 'loja' )}}
								</div>
								<div class="form-group">
									<label>Bairro</label>
									<input type="text" class="form-control" id="brrloja" name="brrloja" value="{{$loja->loja_bairro or old('brrloja')}}" req='true'>
								</div>
								<div class="row">
									<div class="col-md-8">
										<div class="form-group">
											<label>Endereço</label>
											<input type="text" class="form-control" id="endloja" name="endloja" value="{{$loja->loja_logradouro or old('endloja')}}" req='true'>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-4">
											<label>Nº</label>
											<input type="text" class="form-control" id="numloja" name="numloja" value="{{$loja->loja_numero or old('numloja')}}" req='true'>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label>Abertura</label>
											<input type="text" class="form-control" id="abtloja" name="abtloja" value="{{$loja->loja_ab or old('abtloja')}}" req='true'>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Fechamento</label>
											<input type="text" class="form-control" id="fchloja" name="fchloja" value="{{$loja->loja_fec or old('fchloja')}}" req='true'>
										</div>
									</div>
                                                                    
                                                                        <div class="col-md-6">
										<div class="form-group">
											<label>Dias que Abrem</label>
											<select class="selectpicker form-control" multiple data-actions-box="true" name="diasloja[]" req="true">
                                                                                                @foreach($diassemana as $d)
                                                                                                @php
                                                                                                    $selected = '';
                                                                                                    if (!empty($diasemanaloja)){
                                                                                                            $resultado = $diasemanaloja->where('smn_id', descodificaString($d->smn_id))->toArray();
                                                                                                            if( !empty($resultado)) {
                                                                                                                    $selected = 'selected';
                                                                                                            } else {
                                                                                                                    $selected = '';
                                                                                                            }
                                                                                                    }
												@endphp
                                                                                                <option value="{{$d->smn_id}}" {{$selected}}>{{$d->smn_dia}}</option>
                                                                                                @endforeach
                                                                                        </select>
										</div>
									</div>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-8 col-md-offset-2">
									<button id="send" type="submit" class="btn btn-primary">Cadastrar</button>
									@if( isset($loja) && count($loja) > 0 )
									<button type="button" data-toggle="modal" data-target="#modaldesativa" class="btn btn-danger">Desativar</button>
									@endif
								</div>
							</div>
						</form>
                </div>
            </div>
        </div>
    </div>
</div>
@if( isset($loja) && count($loja) > 0 )
<div class="modal fade" tabindex="-1" role="dialog" id="modaldesativa">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Deseja mesmo Desativar essa Loja?</h4>
            </div>
            <div class="modal-body">
                <p>Desativar {{$loja->loja_nome}}</p>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<a type="button" href="{{url("/adm/loja/desativar/" . $id)}}" class="btn btn-danger">Desativar</a>
			</div>
		</div>
	</div>
</div>
@endif

@endsection
<script>
	function consultaCEP( e ) {
		var cep = $( "#ceploja" ).val();

		if( cep != undefined && cep != "" ) {
			// Verifica se na string contem pelo menos um número e executa a consulta
			if( cep.match( ".*[0-9].*" ) ) {
				// Bloqueia botão
				var nomeBotao = $( e ).html();
				$( e ).html( "Carregando..." );
				$( e ).addClass( "disabled" );

				// Faz a requisiçao
				$.ajax( {
					type: 'GET',
					url: "https://api.postmon.com.br/v1/cep/" + cep,
					dataType: 'JSON',
					async: false,
					success: function( data ) {
						$( "#brrloja" ).val( data.bairro );
						$( '#endloja' ).val( data.logradouro );
						$( '#ufloja' ).val( data.estado );
						carregaCidades( "munloja", data.estado, data.cidade );
					},
					error: function( ) {
						toastr.error( "Ocorreu um erro ao consultar o CEP. Verifique se o número foi digitado corretamente.", 'Ops!', '2', 'toast-top-center' );
					},
					complete: function() {
						// Libera botão depois de um pequeno atraso
						setTimeout( function() {
							$( e ).html( nomeBotao );
							$( e ).removeClass( "disabled" );
						}, 300 );
					}
				} );
			}
		} else {
			toastr.warning( "Preencha o campo CEP para fazer a busca" );
		}
	}
</script>