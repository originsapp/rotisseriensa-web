@extends('admin.template.template')

@section('admin')

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Categoria</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Listagem de Categorias</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="table-responsive">

                        <table id="customDatatable" class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th class="column-title">Título</th>
                                    <th class="column-title">Ações</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($categorias as $c)
                                <tr class="even pointer">
                                    <td class=" ">{{$c->ctg_titulo}}</td>
                                    <td class=" last"><a href="{{url('/adm/categoria/alterar/' . $c->ctg_id)}}">Alterar</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
