<?php

/**
 * Função para debugar um array de retorno de dados utilizando o print_r()
 * @param mixed $array Dados que serão debugados.
 * @param boolean $exit (opcional) Caso FALSE desativa o exit da função.
 */
function debug( $array, $exit = true )
{
	echo "<pre>";
	print_r( $array );
	echo "</pre>";

	if( $exit ) {
		exit;
	}
}

/**
 * Retorna um array contendo SIGLA/SIGLA de todos os estados do Brasil
 */
function arrayEstados()
{
	return ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];
}

/**
 * Utilizado nos cadastros de endereço, afim de preencher o Estado e Cidade.
 *
 * @param string $attr Atributos que serao adicionados nos INPUTs
 * @param array $endereco Array com as informações de endereço do Registro (se for uma visualização/edição)
 * <code>
 * 		array(
 * 			'uf' => string, # Código da UF. Ex. 'SP', 'AC', ...
 * 			'cidade' => string # Código da cidade
 * 		)
 * </code>
 * @param array $ufs Array de unidades federativas
 * <code>
 * 		array(
 * 			[0] => 'SP',
 * 			[0] => 'AC'
 * 		)
 * </code>
 * @param array $cidades Array de Cidades
 * <code>
 * 		array(
 * 			array(
 * 				'codigo' => string,
 * 				'nome' => string
 * 			)
 * 		)
 * </code>
 * @param string $suf (opcional) Sufixo que poderá ser adicionado aos campos, caso haja mais de um endereço
 * @param boolean $obrigatorio (opcional) Se FALSE o campo UF e Cidade não fica obrigatório.
 */
function endereco( $attr, $endereco, $ufs, $cidades, $suf = '', $obrigatorio = true )
{
	$spanObrigatorio = $uf = $cid = "";

	if( $endereco ) {
		$uf = $endereco['uf'];
		$cid = $endereco['cidade'];
	}

	if( $obrigatorio ) {
		$spanObrigatorio = '*';
	}
	?>
	<div class="endereco">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">UF<?php echo $spanObrigatorio ?></label>
				<select id="uf<?php echo $suf; ?>" name="uf<?php echo $suf; ?>"  class="form-control" <?php echo $attr; ?> onchange="carregaCidades( 'mun<?php echo $suf; ?>', this.value );" <?php echo $obrigatorio == true ? "req='true'" : '' ?> >
					<option value="">Selecione</option>
					<?php
					foreach( $ufs as $u ) {
						$selected = $uf == $u ? 'selected' : '';
						?>
						<option value="<?php echo $u; ?>" <?php echo $selected; ?>><?php echo $u; ?></option>
						<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Municipio<?php echo $spanObrigatorio ?></label>
				<select name="mun<?php echo $suf; ?>" id="mun<?php echo $suf; ?>" class="form-control selectpicker" <?php echo $attr; ?> data-live-search="true" <?php echo $obrigatorio == true ? "req='true'" : '' ?>>
					<option id="option" value="">Selecione</option>
					<?php
					if( !empty( $cidades ) ) {
						foreach( $cidades as $cidade ) {
							$selected = $cid == $cidade['cid_id'] ? 'selected' : '';
							?>
							<option value="<?php echo codificaString( $cidade['cid_id'] ); ?>" <?php echo $selected; ?>><?php echo $cidade['cid_nome']; ?></option>
							<?php
						}
					}
					?>
				</select>
			</div>
		</div>
	</div>
	<?php
}

/**
 * Códifica uma string.
 * ]
 * @param type $id
 */
function codificaString( $id )
{
	return strrev( base64_encode( $id ) );
}

function descodificaString( $id )
{
	return base64_decode( strrev( $id ) );
}
