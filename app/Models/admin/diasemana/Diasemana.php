<?php

namespace App\Models\admin\diasemana;

use Illuminate\Database\Eloquent\Model;

class Diasemana extends Model
{
    protected $table = 'diassemana';
    
    public $timestamps = false;
    
    protected $fillable = ['smn_dia'];
    
    protected $guard = ['smn_id'];
}
