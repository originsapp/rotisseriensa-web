<?php

namespace App\Models\admin\login;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    
    protected $table = 'usuario';
    
    public $timestamps = false;
    
    protected $fillable = ['usu_login', 'usu_pwd', 'usu_ativo'];
    
    protected $guard = ['usu_id'];
    
}
