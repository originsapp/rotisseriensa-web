<?php

namespace App\Models\admin\menuloja;

use Illuminate\Database\Eloquent\Model;

class Menuloja extends Model
{
    protected $table = 'menu_loja';
    public $timestamps = false;
    protected $fillable = ['loja_id', 'menu_id'];
    protected $guard = ['mlj_id'];
    
}
