<?php

namespace App\Models\admin\menu;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

    protected $table = 'menu';
    public $timestamps = false;
    protected $fillable = ['menu_titulo', 'menu_descricao', 'menu_preco', 'menu_tipopreco', 'ctg_id', 'menu_ativo'];
    protected $guard = ['menu_id'];

}
