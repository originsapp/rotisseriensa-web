<?php

namespace App\Models\admin\loja;

use Illuminate\Database\Eloquent\Model;

class Loja extends Model {

	protected $table = 'loja';
	public $timestamps = false;
	protected $fillable = ['loja_nome', 'loja_logradouro', 'loja_bairro', 'loja_cep', 'loja_numero', 'cid_id', 'loja_ab', 'loja_fec', 'loja_sit'];
	protected $guard = ['loja_id'];

	public function todasLojasAtivas()
	{
		return self::where( 'loja_ativo', 'S' )
				->join( 'cidade', 'cidade.cid_id', '=', 'loja.cid_id' )
				->select( 'loja_id', 'loja_nome', 'loja_logradouro', 'loja_bairro', 'cidade.cid_nome' )->get();
	}

}
