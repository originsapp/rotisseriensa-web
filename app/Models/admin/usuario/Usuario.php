<?php

namespace App\Models\admin\usuario;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    
    protected $table = 'usuario';
    
    public $timestamps = false;
    
    protected $fillable = ['usu_nome', 'usu_sobrenome', 'usu_login', 'usu_pwd', 'usu_tipo', 'usu_ativo'];
    
    protected $guard = ['usu_id'];
    
}
