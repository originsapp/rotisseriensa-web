<?php

namespace App\Models\admin\diasemana_loja;

use Illuminate\Database\Eloquent\Model;

class Diasemanaloja extends Model
{
    protected $table = 'diasemana_loja';
    
    public $timestamps = false;
    
    protected $fillable = ['smn_id', 'loja_id'];
    
    protected $guard = ['dlj_id'];
}
