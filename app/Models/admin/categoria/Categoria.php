<?php

namespace App\Models\admin\categoria;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    
    protected $table = 'categoria';
    
    public $timestamps = false;
    
    protected $fillable = ['ctg_titulo', 'ctg_ativo'];
    
    protected $guard = ['ctg_id'];
    
}
