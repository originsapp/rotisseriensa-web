<?php

namespace App\Models\admin\cardapio;

use Illuminate\Database\Eloquent\Model;

class Cardapio extends Model
{
    
    protected $table = 'cardapio';
    
    public $timestamps = false;
    
    protected $fillable = ['cdp_descricao', 'cdp_salada', 'cdp_data', 'loja_id', 'cdp_sobremesa', 'tipo_id'];
    
    protected $guard = ['cdp_id'];
    
}
