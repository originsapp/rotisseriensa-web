<?php

namespace App\Models\admin\cardapio;

use Illuminate\Database\Eloquent\Model;

class TipoMarmitex extends Model
{
    
    protected $table = 'tipo_marmitex';
    
    public $timestamps = false;
    
    protected $fillable = ['tipo_nome', 'tipo_ativo'];
    
    protected $guard = ['tipo_id'];
    
}
