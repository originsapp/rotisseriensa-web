<?php

namespace App\Models\admin\cardapio;

use Illuminate\Database\Eloquent\Model;

class Mistura extends Model
{
    
    protected $table = 'mistura';
    
    public $timestamps = false;
    
    protected $fillable = ['cdp_id', 'mst_desc', 'mst_opc'];
    
    protected $guard = ['mst_id'];
    
}
