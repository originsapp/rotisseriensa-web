<?php

namespace App\Models\admin\cardapio;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    
    protected $table = 'log_cardapio';
    
    public $timestamps = false;
    
    protected $fillable = ['cdp_id', 'usu_id', 'lcp_dth'];
    
    protected $guard = ['lcp_id'];
    
}
