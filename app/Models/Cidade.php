<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model {

	protected $table = 'cidade';
	public $timestamps = false;
	protected $fillable = ['cid_uf', 'cid_nome', 'cid_codibge'];
	protected $guard = ['cid_id'];

	public function cidadePorEstado( $uf )
	{
		return self::where( 'cid_uf', $uf )->select( 'cid_id', 'cid_nome', 'cid_uf', 'cid_codibge' )->get();
	}

}
