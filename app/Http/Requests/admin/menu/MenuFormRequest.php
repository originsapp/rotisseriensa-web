<?php

namespace App\Http\Requests\admin\menu;

use Illuminate\Foundation\Http\FormRequest;

class MenuFormRequest extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'titulomenu' => 'required|min:1|max:35',
            'precomenu' => 'required|min:1|max:7',
            'categoriamenu' => 'required',
            'tipoprecomenu' => 'required|min:1|max:7',
            'lojasmenu' => 'required'
        ];
    }

    public function messages() {

        return[];
    }

}
