<?php

namespace App\Http\Requests\admin\loja;

use Illuminate\Foundation\Http\FormRequest;

class LojaFormRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        return [
            'nomeloja'          => 'required|min:3|max:35',
            'ceploja'           => 'required|min:9|max:9',
            'munloja'           => 'required|min:2|max:45',
            'brrloja'           => 'required|min:2|max:50',
            'endloja'           => 'required|min:2|max:75',
            'numloja'           => 'required|max:9',
            'abtloja'           => 'required|min:5|max:5',
            'fchloja'           => 'required|min:5|max:5'
        ];

    }

    public function messages()
    {

        return[];

    }
}
