<?php

namespace App\Http\Requests\admin\categoria;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaFormRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

   
    public function rules()
    {
        
        return ['titulocategoria' => 'required|min:3|max:35'];
        
    }
    
    public function messages()
    {
        
        return[];
        
    }
}
