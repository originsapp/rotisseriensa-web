<?php

namespace App\Http\Requests\admin\login;

use Illuminate\Foundation\Http\FormRequest;

class LoginFormRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

   
    public function rules()
    {
        
        return ['loginusuario' => 'required|min:3|max:40',
                'loginsenha' => 'required|min:3|max:40'];
        
    }
    
    public function messages()
    {
        
        return[];
        
    }
}
