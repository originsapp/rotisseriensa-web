<?php

namespace App\Http\Requests\admin\usuario;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioFormRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

   
    public function rules()
    {
        
        return [
            'nomeusuario'           => 'required|min:3|max:20',
            'sobrenomeusuario'      => 'required|min:3|max:20',
            'senhausuario'          => 'required|min:4|max:20'
        ];
        
    }
    
    public function messages()
    {
        
        return[];
        
    }
}