<?php

namespace App\Http\Requests\admin\cardapio;

use Illuminate\Foundation\Http\FormRequest;

class CardapioFormRequest extends FormRequest
{
    
    public function authorize(){
        return true;
    }
    
    public function rules(){
        return [];
    }
    
     public function messages() {

        return[];
    }

}
