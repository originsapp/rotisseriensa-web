<?php

namespace App\Http\Controllers\admin\menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\admin\menu\Menu;
use App\Models\admin\categoria\Categoria;
use App\Models\admin\loja\Loja;
use App\Http\Requests\admin\menu\MenuFormRequest;
use App\Models\admin\menuloja\Menuloja;

class MenuController extends Controller {

    private $menu;
    private $menuloja;

    public function __construct(Menu $menu, Menuloja $menuloja) {
        $this->menu = $menu;
        $this->menuloja = $menuloja;
    }

    public function index(Request $request) {
        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {
            
            return redirect("adm/login");
            
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
            
        }

        $title = "Rotisserie - Listagem de Menu";
        $menus = $this->menu->where('menu_ativo', 'S')
                ->join('categoria', 'categoria.ctg_id', '=', 'menu.ctg_id')
                ->select('menu_id', 'menu_titulo', 'menu_descricao', 'menu_preco', 'menu_tipopreco', 'ctg_titulo')
                ->get();

        for ($i = 0; $i < count($menus); $i++) {
            $menus[$i]["menu_id"] = strrev(base64_encode($menus[$i]["menu_id"]));
        }

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/menu/cntmenu', compact('menus', 'title', 'nome', 'sobrenome', 'tipo'));
    }

    public function create(Request $request, Categoria $categoria, Loja $loja) {
        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {
            
            return redirect("adm/login");
            
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

		// Busca os dados da categoria para preencher o selectoption
        $categoriasOriginal = $categoria->where("ctg_ativo", "S")->pluck('ctg_titulo', 'ctg_id');

		// Criptografa o id da categoria
		$categorias = [];
		foreach( $categoriasOriginal as $key => $value ) {
			$categorias[strrev(base64_encode($key))] = $value;
		}
                
        $lojas = $loja->where('loja_ativo', 'S')->select('loja_nome', 'loja_id')->get();
        for($i = 0; $i < count($lojas); $i++){
            
            $lojas[$i]["loja_id"] = strrev(base64_encode($lojas[$i]["loja_id"]));
            
        }
        
        $title = "Rotisserie - Cadastro de Menu";
        $cdtaltr = "Cadastrar";
        $cdtalt = "Cadastro";

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;
		$categoriaSelecionada = '';

        return view('admin/menu/cdtmenu', compact('title', 'cdtaltr', 'cdtalt', 'nome', 'sobrenome', 'tipo', 'categorias', 'categoriaSelecionada', 'lojas'));
    }

    public function store(MenuFormRequest $request) {
        
        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
            
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
            
        }

        $dados = $request->except(['_token']);

        $dados["categoriamenu"] = base64_decode(strrev($dados["categoriamenu"]));
        
        $datas = [
            "menu_titulo" => $dados["titulomenu"],
            "menu_descricao" => $dados["descricaomenu"],
            "menu_preco" => $dados["precomenu"],
            "ctg_id" => $dados["categoriamenu"],
            "menu_tipopreco" => $dados["tipoprecomenu"],
        ];
        
        for($i = 0; $i < count($dados["lojasmenu"]); $i++){
            $dados["lojasmenu"][$i] = base64_decode(strrev($dados["lojasmenu"][$i]));
        }

        $insert = $this->menu->create($datas);

        for($i = 0; $i < count($dados["lojasmenu"]); $i++){
            
            $dataslojamenu = ["menu_id" => $insert["attributes"]["id"],
                              "loja_id" => $dados["lojasmenu"][$i]];
            
            $this->menuloja->create($dataslojamenu);
            
            unset($dataslojamenu);
            
        }
        
        if (isset($insert)) {
			$request->session()->flash( 'exibeMensagem', [
				'tipo' => 'success',
				'mensagem' => "Menu cadastrado com sucesso",
			] );
            return redirect('/adm/menu/');
        }
    }

    public function show($id, Request $request, Categoria $categoria, Loja $loja) {
        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

        $iddesc = base64_decode(strrev($id));

		// Busca os dados do menu
        $menu = $this->menu->where(['menu_ativo' => 'S', 'menu_id' => $iddesc])
                ->join('categoria', 'categoria.ctg_id', '=', 'menu.ctg_id')
                ->select('menu_id', 'menu_titulo', 'menu_descricao', 'menu_preco', 'menu_tipopreco', 'ctg_titulo', 'menu.ctg_id')
                ->first();

		$menu_loja = $this->menuloja->where('menu_id', $iddesc)->get();

		// Busca os dados da categoria para preencher o selectoption
        $categoriasOriginal = $categoria->where("ctg_ativo", "S")->pluck('ctg_titulo', 'ctg_id');

		// Criptografa o id da categoria
		$categorias = [];
		foreach( $categoriasOriginal as $key => $value ) {
			$categorias[strrev(base64_encode($key))] = $value;
		}
        
		// Categoria cadastrada no menu
		$categoriaSelecionada = strrev(base64_encode($menu['ctg_id']));

        $lojas = $loja->where('loja_ativo', 'S')->select('loja_nome', 'loja_id')->get();
        for($i = 0; $i < count($lojas); $i++){
            
            $lojas[$i]["loja_id"] = strrev(base64_encode($lojas[$i]["loja_id"]));
            
        }
        
        $title = "Rotisserie - Alterar " . $menu['menu_titulo'];
        $cdtaltr = "Alterar";
        $cdtalt = 'Alteração';

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/menu/cdtmenu', compact('title', 'cdtaltr', 'cdtalt', 'menu', 'id', 'nome', 'sobrenome', 'tipo', 'categorias', 'categoriaSelecionada', 'lojas', 'menu_loja'));
    }

    public function update(Request $request, $id) {

	$request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if(empty($valor)){

            return redirect("adm/login");

        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

        $iddesc = base64_decode(strrev($id));

        $dados = $request->except(['_token']);

		$dados["categoriamenu"] = base64_decode(strrev($dados["categoriamenu"]));

        $datas = [
            "menu_titulo" => $dados["titulomenu"],
            "menu_descricao" => $dados["descricaomenu"],
            "menu_preco" => $dados["precomenu"],
            "ctg_id" => $dados["categoriamenu"],
            "menu_tipopreco" => $dados["tipoprecomenu"],
        ];

		$update = $this->menu->where(['menu_ativo' => 'S', 'menu_id' => $iddesc])->update($datas);
        
        $this->menuloja->where("menu_id", $iddesc)->delete();
        
        for($i = 0; $i < count($dados["lojasmenu"]); $i++){
            $dados["lojasmenu"][$i] = base64_decode(strrev($dados["lojasmenu"][$i]));
        }
        
        for($i = 0; $i < count($dados["lojasmenu"]); $i++){
            
            $dataslojamenu = ["menu_id" => $iddesc,
                              "loja_id" => $dados["lojasmenu"][$i]];
            
            $this->menuloja->create($dataslojamenu);
            
            unset($dataslojamenu);
            
        }
        
        if (isset($update)) {
			$request->session()->flash( 'exibeMensagem', [
				'tipo' => 'success',
				'mensagem' => "Menu alterado com sucesso",
			] );
            return redirect('/adm/menu/');
        }
    }

    public function destroy($id, Request $request) {
        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if(empty($valor)){

            return redirect("adm/login");

        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

        $iddesc = base64_decode(strrev($id));

        $desativar = $this->menu->where('menu_id', $iddesc)->update(['menu_ativo' => 'N']);

        if (isset($desativar)) {
			$request->session()->flash( 'exibeMensagem', [
				'tipo' => 'success',
				'mensagem' => "Menu desativado com sucesso",
			] );
			return redirect('/adm/menu/');
        }
    }

}
