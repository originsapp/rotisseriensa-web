<?php

namespace App\Http\Controllers\admin\usuario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\admin\usuario\Usuario;
use App\Http\Requests\admin\usuario\UsuarioFormRequest;

class UsuarioController extends Controller {

    private $usuario;

    public function __construct(Usuario $usuario) {

        $this->usuario = $usuario;
    }

    public function index(Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
            
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
            
        }

        $title = "Rotisserie - Listagem de Usuários";
        $usuarios = $this->usuario->where('usu_ativo', 'S')->select('usu_id', 'usu_nome', 'usu_sobrenome', 'usu_login')->get();

        for ($i = 0; $i < count($usuarios); $i++) {

            $usuarios[$i]["usu_id"] = strrev(base64_encode($usuarios[$i]["usu_id"]));
        }

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/usuario/cntusuario', compact('usuarios', 'title', 'nome', 'sobrenome', 'tipo'));
    }

    public function create(Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
            
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
            
        }
        
        $title = "Rotisserie - Cadastro de Usuários";
        $cdtaltr = "Cadastrar";
        $cdtalt = "Cadastro";

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/usuario/cdtusuario', compact('title', 'cdtaltr', 'cdtalt', 'nome', 'sobrenome', 'tipo'));
    }

    public function store(UsuarioFormRequest $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
            
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
            
        }

        $dados = $request->except(['_token']);

        $login = strtolower(str_replace(" ", "", $dados["nomeusuario"] . $dados["sobrenomeusuario"]));

        $datas = ["usu_nome" => $dados["nomeusuario"], 'usu_sobrenome' => $dados["sobrenomeusuario"], 'usu_tipo' => $dados['tipousuario'], 'usu_login' => $login, 'usu_pwd' => md5($dados["senhausuario"])];

        $insert = $this->usuario->create($datas);

        if (isset($insert)) {
            $request->session()->flash('exibeMensagem', [
                'tipo' => 'success',
                'mensagem' => "Usuário cadastrado com sucesso",
            ]);
            return redirect('/adm/usuario/');
        }
    }

    public function show($id, Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
            
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
            
        }

        $iddesc = base64_decode(strrev($id));

        $usuarios = $this->usuario->where(['usu_ativo' => 'S', 'usu_id' => $iddesc])->select('usu_nome', 'usu_sobrenome', 'usu_tipo')->get();

        $title = "Rotisserie - Alterar " . $usuarios[0]['usu_nome'];
        $cdtaltr = "Alterar";
        $cdtalt = 'Alteração';

        $name = $usuarios[0]['usu_nome'];
        $sobre = $usuarios[0]['usu_sobrenome'];
        $usutip = $usuarios[0]['usu_tipo'];

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/usuario/cdtusuario', compact('title', 'cdtaltr', 'cdtalt', 'usuarios', 'name', 'sobre', 'id', 'nome', 'sobrenome', 'tipo', 'usutip'));
    }

    public function update(UsuarioFormRequest $request, $id) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
            
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
            
        }

        $iddesc = base64_decode(strrev($id));

        $dados = $request->except(['_token']);

        $login = strtolower(str_replace(" ", "", $dados["nomeusuario"] . $dados["sobrenomeusuario"]));

        $datas = ["usu_nome" => $dados["nomeusuario"], 'usu_sobrenome' => $dados["sobrenomeusuario"],'usu_tipo' => $dados['tipousuario'], 'usu_login' => $login, 'usu_pwd' => md5($dados["senhausuario"])];

        $update = $this->usuario->where(['usu_ativo' => 'S', 'usu_id' => $iddesc])->update($datas);

        print_r($update);

        if (isset($update)) {
            $request->session()->flash('exibeMensagem', [
                'tipo' => 'success',
                'mensagem' => "Usuário alterado com sucesso",
            ]);
            return redirect('/adm/usuario/');
        }
    }

    public function destroy($id, Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
            
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
            
        }

        $iddesc = base64_decode(strrev($id));

        $desativar = $this->usuario->where('usu_id', $iddesc)->update(['usu_ativo' => 'N']);

        if (isset($desativar)) {
            $request->session()->flash('exibeMensagem', [
                'tipo' => 'success',
                'mensagem' => "Usuário desativado com sucesso",
            ]);
            return redirect('/adm/usuario/');
        }
    }

}
