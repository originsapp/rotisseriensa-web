<?php

namespace App\Http\Controllers\admin\homepage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomepageController extends Controller
{
    
    public function __construct() {
        
        /*$valor = session('nome');
        if(empty($valor)){
            return redirect("/adm/login");
        }*/
        
    }
    
    public function index(Request $request)
    {
        $title = "Rotisserie NSA - Página Principal";
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        } 
        
        $sobrenome = $request->session()->get('usuariosobrenome');
        $tipo = $request->session()->get('usuariotipo');
        $nome = $valor;
        
        return view("admin/homepage/homepage", compact('nome', 'sobrenome', 'tipo', 'title'));
        
    }
    
}
