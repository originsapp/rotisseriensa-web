<?php

namespace App\Http\Controllers\admin\loja;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\admin\loja\Loja;
use App\Models\Cidade;
use App\Models\admin\diasemana\Diasemana;
use App\Models\admin\diasemana_loja\Diasemanaloja;
use App\Http\Requests\admin\loja\LojaFormRequest;

class LojaController extends Controller {

    private $loja;
    private $cidade;
    private $diasemana;
    private $diasemanaloja;
    
    public function __construct(Loja $loja, Cidade $cidade, Diasemana $diasemana, Diasemanaloja $diasemanaloja) {

        $this->loja = $loja;
        $this->cidade = $cidade;
        $this->diasemana = $diasemana;
        $this->diasemanaloja = $diasemanaloja;
        
    }

    public function index(Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

        $title = "Rotisserie - Listagem de Lojas";
        $lojas = $this->loja->todasLojasAtivas();

        for ($i = 0; $i < count($lojas); $i++) {

            $lojas[$i]["loja_id"] = strrev(base64_encode($lojas[$i]["loja_id"]));
        }
        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/loja/cntloja', compact('lojas', 'title', 'nome', 'sobrenome', 'tipo'));
    }

    public function create(Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

        // Busca os estados e cidades
        $estados = arrayEstados();
        $cidades = $this->cidade->cidadePorEstado('SP');
        
        $diassemana = $this->diasemana->select('smn_id', 'smn_dia')->get();
        
        for ($i = 0; $i < count($diassemana); $i++) {

            $diassemana[$i]["smn_id"] = strrev(base64_encode($diassemana[$i]["smn_id"]));
            
        }
        
        $enderecoSelecionado = [
            'uf' => 'SP',
            'cidade' => '566'
        ];

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;
        $title = "Rotisserie - Cadastro de Loja";
        $cdtaltr = "Cadastrar";
        $cdtalt = "Cadastro";

        return view('admin/loja/cdtloja', compact('title', 'cdtaltr', 'cdtalt', 'nome', 'sobrenome', 'tipo', 'estados', 'cidades', 'enderecoSelecionado', 'diassemana'));
        
    }

    public function store(LojaFormRequest $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

        $dados = $request->except(['_token']);
        
        
        $datas = ["loja_nome" => $dados["nomeloja"],
            "loja_cep" => $dados["ceploja"],
            "cid_id" => descodificaString($dados["munloja"]),
            "loja_bairro" => $dados["brrloja"],
            "loja_logradouro" => $dados["endloja"],
            "loja_numero" => $dados["numloja"],
            "loja_ab" => $dados["abtloja"],
            "loja_fec" => $dados["fchloja"]];

        for($i = 0; $i < count($dados["diasloja"]); $i++){
            $dados["diasloja"][$i] = base64_decode(strrev($dados["diasloja"][$i]));
        }

        $insert = $this->loja->create($datas);

        
        for($i = 0; $i < count($dados["diasloja"]); $i++){
            
            $datasdiasloja = ["loja_id" => $insert["attributes"]["id"],
                              "smn_id" => $dados["diasloja"][$i]];
            
            $this->diasemanaloja->create($datasdiasloja);
            
            unset($datasdiasloja);
            
        }
        if (isset($insert)) {
            $request->session()->flash('exibeMensagem', [
                'tipo' => 'success',
                'mensagem' => "Loja cadastrada com sucesso",
            ]);
            return redirect('/adm/loja/');
        }
    }

    public function show($id, Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

        $iddesc = descodificaString($id);

        $loja = $this->loja->where(['loja_ativo' => 'S', 'loja_id' => $iddesc])
                        ->join('cidade', 'cidade.cid_id', '=', 'loja.cid_id')
                        ->select('loja_nome', 'loja_logradouro', 'loja_bairro', 'loja_cep', 'loja_numero', 'loja_fec', 'loja_ab', 'cidade.cid_id', 'cidade.cid_uf')->first();
        $title = "Rotisserie - Alterar " . $loja['loja_nome'];
        $cdtaltr = "Alterar";
        $cdtalt = 'Alteração';
        
        $diassemana = $this->diasemana->select('smn_id', 'smn_dia')->get();
        
        $diasemanaloja = $this->diasemanaloja->where("loja_id", $iddesc)->get();
        
        for ($i = 0; $i < count($diassemana); $i++) {

            $diassemana[$i]["smn_id"] = strrev(base64_encode($diassemana[$i]["smn_id"]));
            
        }
        // Busca os estados e cidades
        $estados = arrayEstados();
        $cidades = $this->cidade->cidadePorEstado($loja['cid_uf']);

        $enderecoSelecionado = [
            'uf' => $loja['cid_uf'],
            'cidade' => $loja['cid_id']
        ];

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/loja/cdtloja', compact('title', 'cdtaltr', 'cdtalt', 'loja', 'id', 'nome', 'sobrenome', 'tipo', 'estados', 'cidades', 'enderecoSelecionado', 'diassemana', 'diasemanaloja'));
    }

    public function update(LojaFormRequest $request, $id) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

        $iddesc = base64_decode(strrev($id));

        $dados = $request->except(['_token']);

        $datas = ["loja_nome" => $dados["nomeloja"],
            "loja_cep" => $dados["ceploja"],
            "cid_id" => descodificaString($dados["munloja"]),
            "loja_bairro" => $dados["brrloja"],
            "loja_logradouro" => $dados["endloja"],
            "loja_numero" => $dados["numloja"],
            "loja_ab" => $dados["abtloja"],
            "loja_fec" => $dados["fchloja"]];

        $update = $this->loja->where(['loja_ativo' => 'S', 'loja_id' => $iddesc])->update($datas);
        
        $this->diasemanaloja->where("loja_id", $iddesc)->delete();
        
        for($i = 0; $i < count($dados["diasloja"]); $i++){
            $dados["diasloja"][$i] = base64_decode(strrev($dados["diasloja"][$i]));
        }
        
        for($i = 0; $i < count($dados["diasloja"]); $i++){
            
            $datasdiasloja = ["loja_id" => $iddesc,
                              "smn_id" => $dados["diasloja"][$i]];
            
            $this->diasemanaloja->create($datasdiasloja);
            
            unset($datasdiasloja);
            
        }
        
        if (isset($update)) {
            $request->session()->flash('exibeMensagem', [
                'tipo' => 'success',
                'mensagem' => "Loja alterada com sucesso",
            ]);
            return redirect('/adm/loja/');
        }
    }

    public function destroy($id, Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if (empty($valor)) {

            return redirect("adm/login");
        } else if ($tipo != 'A') {

            return redirect("adm/homepage");
        }

        $iddesc = base64_decode(strrev($id));

        $desativar = $this->loja->where('loja_id', $iddesc)->update(['loja_ativo' => 'N']);

        if (isset($desativar)) {
            $request->session()->flash('exibeMensagem', [
                'tipo' => 'success',
                'mensagem' => "Loja desativada com sucesso",
            ]);
            return redirect('/adm/loja/');
        }
    }

}
