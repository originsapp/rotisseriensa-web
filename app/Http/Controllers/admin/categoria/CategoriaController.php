<?php

namespace App\Http\Controllers\admin\categoria;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\admin\categoria\Categoria;
use App\Http\Requests\admin\categoria\CategoriaFormRequest;

class CategoriaController extends Controller
{
    private $categoria;

    public function __construct(Categoria $categoria) {

        $this->categoria = $categoria;

    }

    public function index(Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if(empty($valor)){

            return redirect("adm/login");

        }else if($tipo != 'A'){
            
            return redirect("adm/homepage");
            
        }

        $title = "Rotisserie - Listagem de Categoria";
        $categorias = $this->categoria->where('ctg_ativo', 'S')->select('ctg_id', 'ctg_titulo')->get();

        for ($i = 0; $i < count($categorias); $i++) {

            $categorias[$i]["ctg_id"] = strrev(base64_encode($categorias[$i]["ctg_id"]));

        }

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/categoria/cntcategoria', compact('categorias', 'title', 'nome', 'sobrenome', 'tipo'));


    }

    public function create(Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if(empty($valor)){

            return redirect("adm/login");

        }else if($tipo != 'A'){
            
            return redirect("adm/homepage");
            
        }

        $title = "Rotisserie - Cadastro de Categoria";
        $cdtaltr = "Cadastrar";
        $cdtalt = "Cadastro";

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/categoria/cdtcategoria', compact('title', 'cdtaltr', 'cdtalt', 'nome', 'sobrenome', 'tipo'));

    }

    public function store(CategoriaFormRequest $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if(empty($valor)){

            return redirect("adm/login");

        }else if($tipo != 'A'){
            
            return redirect("adm/homepage");
            
        }

        $dados = $request->except(['_token']);
        $datas = ["ctg_titulo" => $dados["titulocategoria"]];

        $insert = $this->categoria->create($datas);

        if (isset($insert)) {
			$request->session()->flash( 'exibeMensagem', [
				'tipo' => 'success',
				'mensagem' => "Categoria cadastrada com sucesso",
			] );
			return redirect('/adm/categoria/');
        }

    }

    public function show($id, Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if(empty($valor)){

            return redirect("adm/login");

        }else if($tipo != 'A'){
            
            return redirect("adm/homepage");
            
        }

        $iddesc = base64_decode(strrev($id));

        $categorias = $this->categoria->where(['ctg_ativo' => 'S', 'ctg_id' => $iddesc])
                        ->select('ctg_titulo')->get();

        $title = "Rotisserie - Alterar " . $categorias[0]['ctg_titulo'];
        $cdtaltr = "Alterar";
        $cdtalt = 'Alteração';

        $titulo = $categorias[0]['ctg_titulo'];

        $sobrenome = $request->session()->get('usuariosobrenome');
        $nome = $valor;

        return view('admin/categoria/cdtcategoria', compact('title', 'cdtaltr', 'cdtalt', 'categorias', 'titulo', 'id', 'nome', 'sobrenome', 'tipo'));

    }

    public function update(CategoriaFormRequest $request, $id) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if(empty($valor)){

            return redirect("adm/login");

        }else if($tipo != 'A'){
            
            return redirect("adm/homepage");
            
        }

        $iddesc = base64_decode(strrev($id));

        $dados = $request->except(['_token']);

        $datas = ["ctg_titulo" => $dados["titulocategoria"]];

        $update = $this->categoria->where(['ctg_ativo' => 'S', 'ctg_id' => $iddesc])->update($datas);

        if (isset($update)) {
			$request->session()->flash( 'exibeMensagem', [
				'tipo' => 'success',
				'mensagem' => "Categoria alterada com sucesso",
			] );
            return redirect('/adm/categoria/');
        }

    }

    public function destroy($id, Request $request) {

        $request->session()->regenerate();

        $valor = $request->session()->get('usuarionome');
        $tipo = $request->session()->get('usuariotipo');

        if(empty($valor)){

            return redirect("adm/login");

        }else if($tipo != 'A'){
            
            return redirect("adm/homepage");
            
        }

        $iddesc = base64_decode(strrev($id));

        $desativar = $this->categoria->where('ctg_id', $iddesc)->update(['ctg_ativo' => 'N']);

        if (isset($desativar)) {
			$request->session()->flash( 'exibeMensagem', [
				'tipo' => 'success',
				'mensagem' => "Categoria desativada com sucesso",
			] );
			return redirect('/adm/categoria/');
        }

    }

}
