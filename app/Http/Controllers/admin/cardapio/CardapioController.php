<?php

namespace App\Http\Controllers\admin\cardapio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\admin\cardapio\Cardapio;
use App\Models\admin\loja\Loja;
use App\Models\admin\cardapio\Mistura;
use App\Models\admin\cardapio\Log;
use App\Models\admin\cardapio\TipoMarmitex;
use App\Http\Requests\admin\cardapio\CardapioFormRequest;


class CardapioController extends Controller
{
    
    private $cardapio;
    private $loja;
    private $mistura;
    private $log;
    private $tipomarmitex;
    
    public function __construct(Cardapio $cardapio, Loja $loja, Mistura $mistura, Log $log, TipoMarmitex $tipomarmitex) {
        $this->cardapio = $cardapio;
        $this->loja = $loja;
        $this->mistura = $mistura;
        $this->log = $log;
        $this->tipomarmitex = $tipomarmitex;
    }
    
    public function index(Request $request)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        }
        
        $vardata = date('Y-m-d');
        
        $lojas = $this->loja->where('loja_ativo', 'S')->select('loja_id', 'loja_nome')->get();
        
        
        for ($i = 0; $i < count($lojas); $i++) {
            
            $cardapio = $this->cardapio->whereBetween('cdp_data', ["$vardata", "$vardata"])->where('loja_id', $lojas[$i]["loja_id"])->select('cdp_id')->get();
            
            $lojas[$i]["loja_id"] = strrev(base64_encode($lojas[$i]["loja_id"]));
            
            if(count($cardapio)){
                
                $lojas[$i]["situacao"] = "Cadastrado Clique aqui para Alterar";
                
            }else {
                
                $lojas[$i]["situacao"] = "Não Cadastrado Clique aqui para Cadastrar";
                
            }
            
            unset($cardapio);
            
        }
        
        $title = "Rotisserie - Consulta de Cardápio do Dia";
        $sobrenome = $request->session()->get('usuariosobrenome');
        $tipo = $request->session()->get('usuariotipo');
        $nome = $valor;
        
        return view('admin/cardapio/cntcardapiodia', compact('lojas', 'title', 'nome', 'sobrenome', 'tipo'));
        
    }
    
    public function cadastrardia(Request $request, $id)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        }
        
        $title = "Rotisserie - Cadastro de Cardápio";
        $cdtaltr = "Cadastrar";
        $cdtalt = "Cadastro";
        $dia = date('d/m/Y');
        
        $sobrenome = $request->session()->get('usuariosobrenome');
        $tipo = $request->session()->get('usuariotipo');
        $nome = $valor;
        
        $tipomarm = $this->tipomarmitex->where('tipo_ativo', 'S')->select('tipo_nome', 'tipo_id')->get();
        
        for ($i = 0; $i < count($tipomarm); $i++) {

            $tipomarm[$i]["tipo_id"] = strrev(base64_encode($tipomarm[$i]["tipo_id"]));
            
        }
        $idtipo = "==QM";
        
        return view('admin/cardapio/cdtcardapiodia', compact('title', 'nome', 'sobrenome', 'tipo', 'cdtalt', 'cdtaltr', 'tipomarm', 'dia', 'id', 'idtipo'));
        
    }
    
    public function cadastrocdp(CardapioFormRequest $request)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        }
        
        $title = "Rotisserie - Cadastro de Cardápio";
        $cdtaltr = "Cadastrar";
        $cdtalt = "Cadastro";
        
        $sobrenome = $request->session()->get('usuariosobrenome');
        $tipo = $request->session()->get('usuariotipo');
        $nome = $valor;
        
        $tipomarm = $this->tipomarmitex->where('tipo_ativo', 'S')->select('tipo_nome', 'tipo_id')->get();
        
        for ($i = 0; $i < count($tipomarm); $i++) {

            $tipomarm[$i]["tipo_id"] = strrev(base64_encode($tipomarm[$i]["tipo_id"]));
            
        }
        $idtipo = "==QM";
        
        $lojas = $this->loja->where('loja_ativo', 'S')->select('loja_id', 'loja_nome')->get();

        for ($i = 0; $i < count($lojas); $i++) {

            $lojas[$i]["loja_id"] = strrev(base64_encode($lojas[$i]["loja_id"]));
            
        }
        
        return view('admin/cardapio/cdtcardapiodia', compact('title', 'nome', 'sobrenome', 'tipo', 'cdtalt', 'cdtaltr', 'tipomarm', 'dia', 'id', 'idtipo', 'lojas'));
        
    }
    
    public function store(CardapioFormRequest $request)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        }
        
        $dados = $request->except(['_token']);
        
        $datas = ["cdp_descricao" => $dados["desccardapio"],
                  "cdp_salada" => $dados["saladacardapio"],
                  "cdp_sobremesa" => $dados["sobremesacardapio"],
                  "tipo_id" => base64_decode(strrev($dados["especialcardapio"])),
                  "cdp_data" => implode('-', array_reverse(explode('/', $dados["datacardapio"]))),
                  "loja_id" => base64_decode(strrev($dados["lojacardapio"]))];
        
        $insert = $this->cardapio->create($datas);
        
        for($i = 0; $i < count($dados["misturacardapio"]); $i++){
            
            $datasmistura = ["cdp_id" => $insert["attributes"]["id"],
                             "mst_desc" => $dados["misturacardapio"][$i],
                             "mst_opc" => $dados["opcaocardapio"][$i]];
            
            $this->mistura->create($datasmistura);
            
            unset($datasmistura);
            
        }
        
        $dataslogs = ["usu_id" => $request->session()->get('usuarioid'),
                      "cdp_id" => $insert["attributes"]["id"]];
        
        $this->log->create($dataslogs);
        
        return redirect('/adm/cardapio');
        
    }
    
    public function consultacdps(Request $request, $id)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        }
        
        $vardata = date('Y-m-d');
        
        $iddesc = base64_decode(strrev($id));
        
        $cardapio = $this->cardapio
                         ->join('tipo_marmitex', 'tipo_marmitex.tipo_id', '=', 'cardapio.tipo_id')
                         ->join('loja', 'loja.loja_id', '=', 'cardapio.loja_id')
                         ->whereBetween('cdp_data', ["$vardata", "$vardata"])
                         ->where('loja.loja_id', $iddesc)
                         ->select('cdp_id', 'loja_nome', 'tipo_nome', 'loja.loja_id as idloja')->get();
        
        if(count($cardapio)){
            
            for ($i = 0; $i < count($cardapio); $i++) {

                $cardapio[$i]["idloja"] = strrev(base64_encode($cardapio[$i]["idloja"]));
                $cardapio[$i]["cdp_id"] = strrev(base64_encode($cardapio[$i]["cdp_id"]));

            }
            
            $title = "Rotisserie - Consulta de Cardápio";
            $dia = date('d/m/Y');
            
            $sobrenome = $request->session()->get('usuariosobrenome');
            $tipo = $request->session()->get('usuariotipo');
            $nome = $valor;
            
            return view('admin/cardapio/cntcardapioscdts', compact('title', 'nome', 'sobrenome', 'tipo', 'cardapio', 'id'));
            
        }else{
            
            return redirect("adm/cardapio/cadastrodia/$id");
            
        }
        
    }
    
    public function cntcardapio(Request $request)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        }
        
        $vardata = date('Y-m-d');
        $sobrenome = $request->session()->get('usuariosobrenome');
        $tipo = $request->session()->get('usuariotipo');
        $nome = $valor;
        $title = "Rotisserie - Consulta de Cardápios";
        
        $cardapio = $this->cardapio
                         ->join('tipo_marmitex', 'tipo_marmitex.tipo_id', '=', 'cardapio.tipo_id')
                         ->join('loja', 'loja.loja_id', '=', 'cardapio.loja_id')
                          ->where('cdp_data', '>', "$vardata")
                          ->select('cdp_id', 'loja_nome', 'tipo_nome', 'cdp_data')->get();
        
        for($i = 0; $i < count($cardapio); $i++){
            
            $cardapio[$i]['cdp_id'] = strrev(base64_encode($cardapio[$i]['cdp_id']));
            $cardapio[$i]['cdp_data'] = implode('/', array_reverse(explode('-', $cardapio[$i]['cdp_data'])));
            
        }
        
        
        return view('admin/cardapio/cntcardapio', compact('title', 'nome', 'sobrenome', 'tipo', 'cardapio'));
        
    }
    
    public function show(Request $request, $idcdp)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        }
        
        $vardata = date('Y-m-d');
        
        $cdpid = base64_decode(strrev($idcdp));
        
        $cardapio = $this->cardapio
                         ->whereBetween('cdp_data', ["$vardata", "$vardata"])
                         ->where('cdp_id', $cdpid)
                         ->select('cdp_descricao', 'cdp_salada', 'cdp_sobremesa', 'loja_id', 'tipo_id')->get();
        
        $mistura = $this->mistura
                        ->where('cdp_id', $cdpid)
                        ->select('mst_desc', 'mst_opc')->get();
        
        
        $title = "Rotisserie - Cadastro de Cardápio";
        $cdtaltr = "Alterar";
        $cdtalt = "Altera";
        $dia = date('d/m/Y');
        $conta = "0";
        
        $sobrenome = $request->session()->get('usuariosobrenome');
        $tipo = $request->session()->get('usuariotipo');
        $nome = $valor;
        
        $tipomarm = $this->tipomarmitex->where('tipo_ativo', 'S')->select('tipo_nome', 'tipo_id')->get();
        
        for ($i = 0; $i < count($tipomarm); $i++) {
            
            $tipomarm[$i]["tipo_id"] = strrev(base64_encode($tipomarm[$i]["tipo_id"]));

        }
        
        $desc = $cardapio[0]["cdp_descricao"];
        $sal = $cardapio[0]["cdp_salada"];
        $sobr = $cardapio[0]["cdp_sobremesa"];
        $loja = strrev(base64_encode($cardapio[0]["loja_id"]));
        $idtipo = strrev(base64_encode($cardapio[0]["tipo_id"]));
        
        return view('admin/cardapio/cdtcardapiodia', compact('title', 'nome', 'sobrenome', 'tipo', 'cdtalt', 'cdtaltr', 
                                                             'tipomarm', 'dia', 'id', 'mistura', 'desc', 'sal', 'sobr', 
                                                             'loja', 'idtipo', 'conta', 'idcdp'));
        
    }
    
    public function showoth(Request $request, $idcdp)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        }
        
        $vardata = date('Y-m-d');
        
        $cdpid = base64_decode(strrev($idcdp));
        
        $cardapio = $this->cardapio
                         ->where('cdp_id', $cdpid)
                         ->select('cdp_descricao', 'cdp_salada', 'cdp_sobremesa', 'loja_id', 'tipo_id', 'cdp_data')->get();
        
        $cardapio[0]['cdp_data'] = implode('/', array_reverse(explode('-', $cardapio[0]['cdp_data'])));
        
        $mistura = $this->mistura
                        ->where('cdp_id', $cdpid)
                        ->select('mst_desc', 'mst_opc')->get();
        
        $title = "Rotisserie - Cadastro de Cardápio";
        $cdtaltr = "Alterar";
        $cdtalt = "Altera";
        $conta = "0";
        
        $sobrenome = $request->session()->get('usuariosobrenome');
        $tipo = $request->session()->get('usuariotipo');
        $nome = $valor;
        
        $tipomarm = $this->tipomarmitex->where('tipo_ativo', 'S')->select('tipo_nome', 'tipo_id')->get();
        
        for ($i = 0; $i < count($tipomarm); $i++) {
            
            $tipomarm[$i]["tipo_id"] = strrev(base64_encode($tipomarm[$i]["tipo_id"]));

        }
        
        $lojas = $this->loja->where('loja_ativo', 'S')->select('loja_id', 'loja_nome')->get();
        
        for($i = 0; $i < count($lojas); $i++){
            $lojas[$i]["loja_id"] = strrev(base64_encode($lojas[$i]["loja_id"]));
        }
        
        $desc = $cardapio[0]["cdp_descricao"];
        $sal = $cardapio[0]["cdp_salada"];
        $sobr = $cardapio[0]["cdp_sobremesa"];
        $loja = strrev(base64_encode($cardapio[0]["loja_id"]));
        $idtipo = strrev(base64_encode($cardapio[0]["tipo_id"]));
        $data = $cardapio[0]['cdp_data'];
        
        return view('admin/cardapio/cdtcardapiodia', compact('title', 'nome', 'sobrenome', 'tipo', 'cdtalt', 'cdtaltr', 
                                                             'tipomarm', 'dia', 'id', 'mistura', 'desc', 'sal', 'sobr', 
                                                             'loja', 'idtipo', 'conta', 'idcdp', 'lojas', 'data'));
        
    }
    
    public function update(CardapioFormRequest $request, $idcdp)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return redirect("adm/login");
            
        }
        $iddesccdp = base64_decode(strrev($idcdp));
        $dados = $request->except(['_token']);
        
        $datas = ["cdp_descricao" => $dados["desccardapio"],
                  "cdp_salada" => $dados["saladacardapio"],
                  "cdp_sobremesa" => $dados["sobremesacardapio"],
                  "tipo_id" => base64_decode(strrev($dados["especialcardapio"])),
                  "cdp_data" => implode('-', array_reverse(explode('/', $dados["datacardapio"]))),
                  "loja_id" => base64_decode(strrev($dados["lojacardapio"]))];
        
        $this->cardapio->where("cdp_id", $iddesccdp)->update($datas);
        
        $this->mistura->where("cdp_id", $iddesccdp)->delete();
        
        for($i = 0; $i < count($dados["misturacardapio"]); $i++){
            
            $datasmistura = ["cdp_id" => $iddesccdp,
                             "mst_desc" => $dados["misturacardapio"][$i],
                             "mst_opc" => $dados["opcaocardapio"][$i]];
            
            $this->mistura->create($datasmistura);
            
            unset($datasmistura);
            
        }
        
        $dataslogs = ["usu_id" => $request->session()->get('usuarioid'),
                      "cdp_id" => $iddesccdp];
        
        $this->log->create($dataslogs);
        
        return redirect('/adm/cardapio');
        
    }
    
}
