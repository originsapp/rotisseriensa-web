<?php

namespace App\Http\Controllers\admin\login;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\admin\login\Login;
use App\Http\Requests\admin\login\LoginFormRequest;

class LoginController extends Controller
{
    
    private $usuario;   
    
    public function __construct(Login $usuario) {
        
        $this->usuario = $usuario;
        
    }
    
    public function index(Request $request)
    {
        
        $request->session()->regenerate();
        
        $valor = $request->session()->get('usuarionome');
        
        if(empty($valor)){
            
            return view("admin/login/login");
            
        } else  {

            return redirect("adm/homepage"); 

        }
        
        return view("admin/login/login");
        
    }
    
    public function logar(LoginFormRequest $request)
    {
        
        $dados = $request->except(['_token']);
        
        $consulta = $this->usuario->where(['usu_login' => $dados["loginusuario"], 'usu_pwd' => md5($dados["loginsenha"])])->select('usu_id', 'usu_nome', 'usu_sobrenome', 'usu_tipo')->get();
        
        if(!empty($consulta[0])){
            
            $request->session()
                    ->put(['usuarionome' => $consulta[0]["usu_nome"], 
                           'usuariosobrenome' => $consulta[0]["usu_sobrenome"], 
                           'usuarioid' => $consulta[0]["usu_id"],
                           'usuariotipo' => $consulta[0]["usu_tipo"]]);
            return redirect("/adm/homepage");
            
        } else {
            
            return redirect("/adm/login");
            
        }
        
    }
    
    public function logout(Request $request){
        
        $request->session()->flush();
        
        return redirect("/adm/login");
        
    }
    
}
