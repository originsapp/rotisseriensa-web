<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cidade;

class CidadeController extends Controller {

	private $cidade;

	public function __construct( Cidade $cidade )
	{
		$this->cidade = $cidade;
	}

	public function montaComboBox( $uf )
	{
		$cidades = [];
		if( !empty( $uf ) ) {
			$cidades = $this->cidade->cidadePorEstado( $uf );
		}

		foreach( $cidades as $c ) {
			echo "<option value='" . codificaString( $c['cid_id'] ) . "'>" . $c['cid_nome'] . '</option>';
		}
	}

}
