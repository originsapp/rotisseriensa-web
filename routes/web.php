<?php

//-------------------------------SITE PRINCIPAL---------------------------------

//index
Route::get('/', function () {
    return view('welcome');
});

//---------------------------FIM SITE PRINCIPAL---------------------------------



//--------------------------------Sistema---------------------------------------

//index
Route::get('/adm', function(){
    return redirect('/adm/login');
});

//login
Route::get('/adm/login', 'admin\login\LoginController@index')->name('login');
Route::post('/adm/login/logar', 'admin\login\LoginController@logar');
Route::get('/adm/login/logout', 'admin\login\LoginController@logout');

//homepage
Route::get('/adm/homepage', 'admin\homepage\HomepageController@index');

//loja
Route::group(['prefix' => 'adm/loja', 'namespace' => 'admin\loja'], function(){
    Route::get('/', 'LojaController@index');
    Route::get('/cadastrar', 'LojaController@create');
    Route::post('/cadastrar/cdt', 'LojaController@store');
    Route::get('/alterar/{id}', 'LojaController@show');
    Route::post('/alterar/alt/{id}', 'LojaController@update');
    Route::get('/desativar/{id}', 'LojaController@destroy');
    Route::get('/cep/{cep}', 'LojaController@cepajax');
});

//categoria
Route::group(['prefix' => 'adm/categoria', 'namespace' => 'admin\categoria'], function(){
    Route::get('/', 'CategoriaController@index');
    Route::get('/cadastrar', 'CategoriaController@create');
    Route::post('/cadastrar/cdt', 'CategoriaController@store');
    Route::get('/alterar/{id}', 'CategoriaController@show');
    Route::post('/alterar/alt/{id}', 'CategoriaController@update');
    Route::get('/desativar/{id}', 'CategoriaController@destroy');
});

//usuario
Route::group(['prefix' => 'adm/usuario', 'namespace' => 'admin\usuario'], function(){
    Route::get('/', 'UsuarioController@index');
    Route::get('/cadastrar', 'UsuarioController@create');
    Route::post('/cadastrar/cdt', 'UsuarioController@store');
    Route::get('/alterar/{id}', 'UsuarioController@show');
    Route::post('/alterar/alt/{id}', 'UsuarioController@update');
    Route::get('/desativar/{id}', 'UsuarioController@destroy');
});

//menu
Route::group(['prefix' => 'adm/menu', 'namespace' => 'admin\menu'], function(){
    Route::get('/', 'MenuController@index');
    Route::get('/cadastrar', 'MenuController@create');
    Route::post('/cadastrar/cdt', 'MenuController@store');
    Route::get('/alterar/{id}', 'MenuController@show');
    Route::post('/alterar/alt/{id}', 'MenuController@update');
    Route::get('/desativar/{id}', 'MenuController@destroy');
});
//cidade ajax
Route::group(['prefix' => 'cidade'], function(){
    Route::get('/montaComboBox/{uf}', 'CidadeController@montaComboBox');
});

    
//cardapio
Route::group(['prefix' => 'adm/cardapio', 'namespace' => 'admin\cardapio'], function(){
    Route::get('/', 'CardapioController@index');
    Route::get('/consultar', 'CardapioController@cntcardapio');
    Route::get('/alterar/{id}', 'CardapioController@showoth');
    Route::get('/consultar/{id}', 'CardapioController@consultacdps');
    Route::get('/cadastrodia/{id}', 'CardapioController@cadastrardia');
    Route::get('/cadastrar', 'CardapioController@cadastrocdp');
    Route::get('/exibiralt', 'CardapioController@');
    Route::post('/cadastrar/cdt', 'CardapioController@store');
    Route::get('/exibir/{idcdp}', 'CardapioController@show');
    Route::post('/alterar/alt/{idcdp}', 'CardapioController@update');
});

//---------------------------FIM SISTEMA----------------------------------------
